﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="WebApplication1.dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Data Diri</title>
    <link rel="stylesheet" type="text/css" href="assets/css/_font.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/myForm.css"/>
</head>
<body>
    <div class="Header">
        <div class="Home">
        <a href="#">
                <img src="assets/img/teamL.png" alt="Logo"/>
            <label class="Logo">Loginin Gan</label>
        </a>
        </div>
        <div class="HeaderLink">
            <a href="index.aspx">Keluar</a>
        </div>
    </div>
    <div class="Konten">
        <div id="Kontainer" class="Kontainer">
                <h2 id="Welcome" runat="server" class="Welcome">Selamat datang kembali, </h2>
                <img class="UserPic" src="assets/img/user.png" alt="assets/img/user.png"/>
                <label id="NPM" runat="server" class="MyID"></label>
                <div id="tabelDiv" runat="server"></div>
        </div>
    </div>
    <div class="KontainerBg"></div>
    <div class="Footer">
        <img src="assets/img/teamL.png" alt="Team"/>
        <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        <img src="assets/img/user.png" alt="User"/>
        <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </div>
</body>
<script type="text/javascript" src="assets/js/_data.js"></script>
<!--<script type="text/javascript" src="assets/js/_dataGet.js"></script>!-->
</html>

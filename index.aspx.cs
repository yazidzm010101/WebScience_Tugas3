﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Validasi(object sender, EventArgs e)
        {
            //Mendapatkan username dan password dari Request
            string userIn = Request["username"];
            string passIn = Request["password"];

            //Deklarasi Query SQL
            string querySQL = String.Format("SELECT npm FROM users WHERE npm=\"{0}\" and password=\"{1}\";", userIn,passIn);

            //Buat Koneksi SQL
            MySqlConnection koneksiSQL = new MySqlConnection();

            //Memanggil Method KoneksiSql() yang akan menghubungkan dengan account sql tertentu kemudian mereturnnya ke dalam variabel
            koneksiSQL = KoneksiSql();

            //Deklarasi SQLCommand menggunakan Query dan Koneksi yang telah ditentukan, kemudian menghubungkannya
            MySqlCommand perintahSQL = new MySqlCommand(querySQL, koneksiSQL); koneksiSQL.Open();

            //Mengexecute SQLCommand dan me-Retrieve data 
            MySqlDataReader dataSQL = perintahSQL.ExecuteReader();

            //Apabila Username dan Password Sesuai(Data ditemukan) maka user login
            if (dataSQL.HasRows)
            {
                Response.Redirect("dashboard.aspx?username=" + userIn);
            }
        }

        protected MySqlConnection KoneksiSql()
        {
            String QueryKoneksi = String.Format("server={0};user id={1};password={2};database=webscience;", "localhost", "admin", "admin");
            MySqlConnection myKoneksi = new MySqlConnection(QueryKoneksi);
            return myKoneksi;
        }
    }
}
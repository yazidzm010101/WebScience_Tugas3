﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace WebApplication1
{
    public partial class dashboard : System.Web.UI.Page
    {
        MySqlConnection koneksiSQL;
        String userIn;
        List<string> DataDiri = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.userIn = Request.QueryString["username"];
            AmbilData();
            GenerateKonten();
        }

        protected MySqlConnection KoneksiSql()
        {
            String QueryKoneksi = String.Format("server={0};user id={1};database=webscience;","localhost","root");
            MySqlConnection myKoneksi = new MySqlConnection(QueryKoneksi);
            return myKoneksi;
        }

        public void AmbilData()
        {
            //Membuat instance dari method KoneksiSQL sehingga dapat terhubung ke MySQL
            this.koneksiSQL = KoneksiSql();

            //Query untuk mencari data user
            String querySQL = String.Format("SELECT nama,kelas,ttl,alamat FROM users WHERE npm=\"{0}\";",userIn); 

            //Membuat SQLCommand menggunakan Query dan Koneksi yang telah ditentukan
            MySqlCommand comm = new MySqlCommand(querySQL, this.koneksiSQL);

            //Menghubungkan Koneksi tersebut
            this.koneksiSQL.Open();

            //Mengexecute SQLCommand dan me-Retrieve data 
            MySqlDataReader resultsSQL = comm.ExecuteReader();

            //Jika Ada Data(Ketemu)
            if (resultsSQL.HasRows)
            {
                //Nge-Get data dari SQL ke ArrayList DataDiri
                while (resultsSQL.Read())
                {
                    this.DataDiri.Add("Nama");
                    this.DataDiri.Add(resultsSQL[0].ToString());
                    this.DataDiri.Add("Kelas");
                    this.DataDiri.Add(resultsSQL[1].ToString());
                    this.DataDiri.Add("Tempat, Tanggal Lahir");
                    this.DataDiri.Add(resultsSQL[2].ToString());
                    this.DataDiri.Add("Alamat");
                    this.DataDiri.Add(resultsSQL[3].ToString());
                }
            }
        }

       protected void GenerateKonten()//Buat Generate Konten
        {
            //Hypertext tabel yg udh di generate disimpen ke dalam FormattedString
            String myHtml;

            //Pengontrol while
            int i = 0;

            //Pembuka Tag Table
            myHtml = "<table class=\"MyTable\">";

            //Perulangan ngebuat Hypertext Row
            while(i < DataDiri.Count)
            {
                myHtml += String.Format("<tr><th>{0}</th><td>{1}</td></tr>",DataDiri[i],DataDiri[i+1]);
                i += 2;
            }

            //Penutup Tag Table
            myHtml += "</table>";

            //Mengubah innerHtml dari elemen yg akan diisi menjadi String yg berisi Hypertext Tabel tadi
            Welcome.InnerHtml += DataDiri[1];
            NPM.InnerHtml = "NPM. "+userIn;
            tabelDiv.InnerHtml += myHtml;
        }

    }
}
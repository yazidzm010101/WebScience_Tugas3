﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApplication1.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Selamat Datang</title>
    <link rel="stylesheet" type="text/css" href="assets/css/_font.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/myForm.css"/>
</head>
<body>
    <div class="Header">
        <div class="Home">
        <a href="#">
                <img src="assets/img/teamL.png" alt="Logo"/>
            <label class="Logo">Loginin Gan</label>
        </a>
        </div>
        <div class="HeaderLink">
            <a href="index.aspx">Masuk</a>
        </div>
    </div>
    <div class="Kontainer" runat="server">
            <form class="MyForm" name="MyForm" runat="server">
                <div class="Title">
                        <img src="assets/img/team.png" alt="Logo"/>
                        <label class="Logo">Loginin Gan</label>
                </div>
                <label class="MyLabel">NPM</label>
                <input class="MyInput" type="text" id="username" runat="server"/>
                <label class="MyLabel">Kata Sandi</label>
                <input class="MyInput" type="password" id="password" runat="server"/>
                <br/>
                <asp:button type="submit" id="Submit" class="MySubmit" onclick="Validasi" runat="server" Text="Masuk"/>
            </form>
    </div>
    <div class="KontainerBg"></div>
    <div class="Footer">
        <img src="assets/img/teamL.png" alt="Team"/>
        <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        <img src="assets/img/user.png" alt="User"/>
        <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </div>
</body>
</html>
